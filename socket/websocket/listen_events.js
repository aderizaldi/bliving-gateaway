const event_connection = (ws) => {
  let subs = {
    id: 1,
    type: "subscribe_events",
    event_type: "state_changed",
  };
  ws.send(JSON.stringify(subs));
};

module.exports = event_connection;
