const auth_connection = (ws) => {
  let auth = {
    type: "auth",
    access_token: process.env.HOME_ASSISTANT_TOKEN,
  };
  console.log("Websocket Authenticate Proccess..");
  ws.send(JSON.stringify(auth));
};

module.exports = auth_connection;
