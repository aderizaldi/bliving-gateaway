const socket = require("socket.io-client/lib/socket");
const socket_io = require("../socket_io");

var run_websocket = (client, client_io) => {
  client.start();
  client.on("connect", function () {
    console.log("WebSocket Client Connected");
    const auth_connection = require("../websocket/authentication");
    auth_connection(client);

    //connect to socket io Bliving when websocket home assistant is ready
    socket_io.run_socket_io(client_io);
  });

  client.on("reconnect", function () {
    console.log("Reconnecting...");
    socket_io.disconnect_connection(client_io);
  });

  client.on("destroyed", function (event) {
    console.log("WebSocket Client Disconnected", event.code, event.reason);
    socket_io.disconnect_connection(client_io);
    client = null;
  });
};

module.exports = run_websocket;
