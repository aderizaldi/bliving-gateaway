const service = require("../services");

let id = 100;
let entity_id = null;

const app = (ws, io) => {
  //Socket IO
  //Trigger device
  io.on("triggerDevice", async (data) => {
    if (data.entity_id && data.state) {
      //Get type of entity
      let type = data.entity_id.split(".", 1);

      if (type[0] == "switch") {
        //Trigger Switch
        let data_request = {
          id: id,
          type: "call_service",
          domain: "switch",
          service: data.state == "on" ? "turn_on" : "turn_off",
          target: {
            entity_id: data.entity_id,
          },
        };
        id = id + 1;
        entity_id = data.entity_id;
        ws.send(JSON.stringify(data_request));
      } else if (type[0] == "light") {
        //Trigger Light
        let data_request;
        if (data.state == "on") {
          data_request = {
            id: id,
            type: "call_service",
            domain: "light",
            service: "turn_on",
            service_data: {
              brightness: data.brightness,
              entity_id: data.entity_id,
            },
          };
        } else {
          data_request = {
            id: id,
            type: "call_service",
            domain: "light",
            service: "turn_off",
            service_data: {
              entity_id: data.entity_id,
            },
          };
        }
        id = id + 1;
        entity_id = data.entity_id;
        ws.send(JSON.stringify(data_request));
      }
    }
  });

  //Get all device on gateway
  io.on("initializeState", async (data) => {
    if (data.idBliving) {
      let callback = await service.getState(data);
      if (callback) {
        io.emit("feedbackInitializeState", callback);
      }
    } else {
      io.emit("feedbackInitializeState", {
        status: false,
        error: "Message incorrectly formatted",
      });
    }
  });

  //schedule trigger device
  io.on("syncQuickAction", async (data) => {
    if (data.method == "add" || data.method == "update") {
      if (!data.data.condition) {
        let date = new Date();
        let timenow = `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
        if (
          new Date("1/1/1999 " + timenow) >=
          new Date("1/1/1999 " + data.data.trigger.at)
        )
          date.setDate(date.getDate() + 1);
        let day =
          date.getDate() < 10 ? `0${date.getDate()}` : `${date.getDate()}`;
        let month =
          date.getMonth() + 1 < 10
            ? `0${date.getMonth() + 1}`
            : `${date.getMonth() + 1}`;
        let year = date.getFullYear();
        let datenow = `${year}-${month}-${day}`;

        data.data.condition = {
          condition: "template",
          value_template: `{{ now().strftime('%Y-%m-%d') == '${datenow}' }}`,
        };
      }
      let callback = await service.scheduleTrigger.createSchedule(data.data);
      if (callback) {
        io.emit("feedbackSyncQuickAction", callback);
      } else {
        io.emit("feedbackSyncQuickAction", {
          status: false,
          error: "Message incorrectly formatted",
        });
      }
    } else if (data.method == "delete") {
      let callback = await service.scheduleTrigger.deleteSchedule(data.data);
      if (callback) {
        io.emit("feedbackSyncQuickAction", callback);
      } else {
        io.emit("feedbackSyncQuickAction", {
          status: false,
          error: "Message incorrectly formatted",
        });
      }
    }
  });

  //Websocket
  //Listen from websocket Home Assistant
  ws.on("message", (message) => {
    let data = JSON.parse(message);

    //Subcribe listen event state changed
    if (data.type == "auth_ok") {
      console.log("Success authenticated to websocket Home Assistant!");
      const event_connection = require("./websocket/listen_events");
      event_connection(ws, data);
    }

    //Failed listen event
    if (data.id == 1 && data.type == "result" && data.success != true) {
      console.error(data);
    }

    //Failed Trigger
    if (data.id >= 100 && data.type == "result" && data.success == false) {
      let error_response = {
        success: data.success,
        message: "Failed to trigger device!",
        idEntity: entity_id,
        error: data.error,
      };
      io.emit("feedbackStateChanged", error_response);
    }

    //listen event
    if (data.id == 1 && data.type == "event") {
      //filter switch
      let reg = new RegExp(process.env.FILTER_REGEX_DEVICES);
      let filter = data.event.data.entity_id.match(reg);

      if (filter) {
        let data_response = {
          idEntity: data.event.data.entity_id,
        };
        let oldState = {
          state: data.event.data.old_state
            ? data.event.data.old_state.state
            : null,
          attributes: data.event.data.old_state
            ? data.event.data.old_state.attributes
            : {},
        };
        let newState = {
          state: data.event.data.new_state
            ? data.event.data.new_state.state
            : null,
          attributes: data.event.data.new_state
            ? data.event.data.new_state.attributes
            : {},
        };
        data_response.oldState = oldState;
        data_response.newState = newState;

        //emit to Socket IO
        io.emit("feedbackStateChanged", data_response);
      }
    }
  });
};

module.exports = app;
