const disconnect_connection = (io) => {
  io.disconnect();
};

module.exports = disconnect_connection;
