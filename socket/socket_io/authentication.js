const auth_connection = (io) => {
  io.on("authenticated", () => {
    console.log("Success authenticated to socket.io BLiving!");
  }).emit("authenticate", { token: process.env.BLIVING_TOKEN }); //send the jwt
};

module.exports = auth_connection;
