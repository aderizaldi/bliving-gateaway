const run_socket_io = (io) => {
  io.connect();
  io.on("connect", () => {
    console.log(
      "Socket.io Cient Connected\nSocket.io Authenticate Proccess ..."
    );
    const auth_connection = require("./authentication");
    auth_connection(io);
  });

  io.on("connect_error", (data) => {
    console.log(`Socket.io Error = ${data.message}\nReconnecting...`);
  });

  io.on("disconnect", () => {
    console.log("Socket.io connection closed");
  });
};

module.exports = run_socket_io;
