const triggerDevice = require("./triggerDevice");
const getState = require("./getState");
const scheduleTrigger = require("./scheduleTrigger");

module.exports = {
  triggerDevice,
  getState,
  scheduleTrigger,
};
