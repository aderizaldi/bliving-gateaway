const axios = require("axios");

const createSchedule = async (data_request) => {
  try {
    let config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${process.env.HOME_ASSISTANT_TOKEN}`,
      },
    };
    let response = await axios.post(
      `${process.env.HOME_ASSISTANT_RESTAPI_URL}/config/automation/config/${data_request.id}`,
      data_request,
      config
    );
    response.data.data = data_request;
    response.data.success = true;
    return response.data;
  } catch (error) {
    console.error(error);
    return {
      success: false,
      error: error,
    };
    // return false
  }
};

const deleteSchedule = async (data_request) => {
  try {
    let config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${process.env.HOME_ASSISTANT_TOKEN}`,
      },
    };
    let response = await axios.delete(
      `${process.env.HOME_ASSISTANT_RESTAPI_URL}/config/automation/config/${data_request.id}`,
      config
    );
    response.data.data = data_request;
    response.data.success = true;
    return response.data;
  } catch (error) {
    console.error(error);
    return {
      success: false,
      error: error,
    };
    // return false
  }
};

module.exports = {
  createSchedule,
  deleteSchedule,
};
