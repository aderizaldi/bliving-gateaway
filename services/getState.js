const axios = require("axios");

const getState = async (data_request) => {
  try {
    let config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${process.env.HOME_ASSISTANT_TOKEN}`,
      },
    };
    let response = await axios.get(
      `${process.env.HOME_ASSISTANT_RESTAPI_URL}/states`,
      config
    );
    let reg = new RegExp(process.env.FILTER_REGEX_DEVICES);
    let data_array = response.data.filter(({ entity_id }) =>
      entity_id.match(reg)
    );

    let data = {
      success: true,
      idBliving: data_request.idBliving,
      idUser: data_request.idUser || null,
      data: data_array || [],
    };
    return data;
  } catch (error) {
    console.error(error);
    return {
      success: false,
      error: error,
    };
  }
};

module.exports = getState;
