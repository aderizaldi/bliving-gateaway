const axios = require("axios");

const triggerDevice = async (entity_id, state, attributes) => {
  try {
    let config = {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${process.env.HOME_ASSISTANT_TOKEN}`,
      },
    };
    let payload = {
      state: state,
      attributes: attributes || {},
    };
    let response = await axios.post(
      `${process.env.HOME_ASSISTANT_RESTAPI_URL}/states/${entity_id}`,
      payload,
      config
    );
    response.data.success = true;
    return response.data;
  } catch (error) {
    console.error(error);
    return {
      success: false,
      entity_id: entity_id,
      state: state,
      error: error,
    };
    // return false
  }
};

module.exports = triggerDevice;
