require("dotenv/config");
let io = require("socket.io-client")(process.env.BLIVING_SOCKET_IO_URL, {
  transports: ["websocket", "polling"],
});

var WebSocketClient = require("ws-reconnect");
var ws = new WebSocketClient(process.env.HOME_ASSISTANT_WEBSOCKET_URL, {
  retryCount: 2880,
  reconnectInterval: 30,
});

//websocket
const websocket = require("./socket/websocket");
const integration = require("./socket/integration");

//socket
websocket.run_websocket(ws, io);
integration(ws, io);
